package imagebrowser.ui.swing;

import imagebrowser.control.CommandMap;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ApplicationFrame extends JFrame {
    
    private CommandMap commands;
    
    public ApplicationFrame(ImagePanel viewer, CommandMap commands) {
        super("Image Browser");
        this.commands = commands;
        this.setSize(300, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.add(viewer);
        this.add(createToolbar(), BorderLayout.SOUTH);
    }
    
    private JPanel createToolbar() {
        JPanel panel = new JPanel();
        panel.add(createButton("Prev"));
        panel.add(createButton("Next"));
        return panel;
    }

    private JButton createButton(String label) {
        JButton button = new JButton(label);
        button.addActionListener(commands.get(label));
        return button;
    }
}
