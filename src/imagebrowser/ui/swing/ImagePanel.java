package imagebrowser.ui.swing;

import imagebrowser.model.Image;
import imagebrowser.ui.ImageViewer;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ImagePanel extends JPanel implements ImageViewer {
    
    private Image image;
    
    private int offset;
    
    public ImagePanel() {
        this.offset = 0;
        new MouseEventsHandler(this).addMouseEventsHandler();
    }
    
    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void setImage(Image image) {
        this.image = image;
        repaint();
    }

    @Override
    public void paint(Graphics graphics) {
        if (image == null) return;
        super.paint(graphics); 
        graphics.drawImage(getBufferedImage(image), offset, 0, null);
        if (offset == 0) return;
        if (offset < 0) {
            graphics.drawImage(getBufferedImage(image.getNextImage()), image.getBitmap().getWidth() + offset, 0, null);
        }
        if (offset > 0) {
            graphics.drawImage(getBufferedImage(image.getPrevImage()), offset - image.getBitmap().getWidth() , 0, null);
        }
    }

    private BufferedImage getBufferedImage(Image image) {
        SwingBitmap bitmap = (SwingBitmap) image.getBitmap();
        return bitmap.getBufferedImage();
    }

    @Override
    public void getNext() {
       image = image.getNextImage();
       repaint();
    }

    @Override
    public void getPrev() {
        image = image.getPrevImage();
        repaint();
    }
    
    

}
