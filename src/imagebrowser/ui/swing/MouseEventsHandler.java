/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imagebrowser.ui.swing;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author Andrea
 */
public class MouseEventsHandler{
            
    private int offset = 0;
    private int initialX;
    private final ImagePanel panel;

    public MouseEventsHandler(ImagePanel panel) {
        this.panel = panel;
    }
    
    public void addMouseEventsHandler(){
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
                initialX = me.getX();
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                if (offset > panel.getImage().getBitmap().getWidth() / 2)
                    panel.getPrev();
                else if (offset < - panel.getImage().getBitmap().getWidth() / 2)
                    panel.getNext();
                offset = 0;
                panel.repaint();
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        });
        panel.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent me) {
                offset = me.getX() - initialX;
            }

            @Override
            public void mouseMoved(MouseEvent me) {
            }
        });
    }
}
