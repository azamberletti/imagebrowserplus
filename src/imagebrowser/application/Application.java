package imagebrowser.application;

import imagebrowser.control.CommandMap;
import imagebrowser.ui.swing.ApplicationFrame;
import imagebrowser.model.Image;
import imagebrowser.persistence.ImageListLoader;
import imagebrowser.persistence.file.FileImageListLoader;
import imagebrowser.ui.swing.ImagePanel;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        ImageListLoader loader = new FileImageListLoader(PATH);
        List<Image> list = loader.load();
        ImagePanel viewer = new ImagePanel();
        viewer.setImage(list.get(0));
        ApplicationFrame frame = new ApplicationFrame(viewer, new CommandMap(viewer));
        frame.setVisible(true);
    }
    
    private static final String PATH = "/Users/Andrea/Dropbox/Screenshot";
}
