/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imagebrowser.control;

import imagebrowser.ui.ImageViewer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Andrea
 */
public abstract class BrowseCommand implements ActionListener{

    private final ImageViewer viewer;

    public BrowseCommand(ImageViewer viewer) {
        this.viewer = viewer;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        execute();
    }
    
    public abstract void execute();

    public ImageViewer getViewer() {
        return viewer;
    }
}
