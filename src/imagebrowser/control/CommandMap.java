/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imagebrowser.control;

import imagebrowser.ui.ImageViewer;
import java.util.HashMap;

/**
 *
 * @author Andrea
 */
public class CommandMap extends HashMap<String, BrowseCommand>{
    
    private static final String COMMAND_NEXT = "Next";
    private static final String COMMAND_PREV = "Prev";
    
    public CommandMap(ImageViewer viewer){
        put(COMMAND_NEXT, new NextImageCommand(viewer));
        put(COMMAND_PREV, new PrevImageCommand(viewer));
    }
}
