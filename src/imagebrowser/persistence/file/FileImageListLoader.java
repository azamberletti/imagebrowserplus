package imagebrowser.persistence.file;

import imagebrowser.model.Image;
import imagebrowser.persistence.ImageListLoader;
import imagebrowser.persistence.ProxyImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileImageListLoader extends ImageListLoader {

    private String path;

    public FileImageListLoader(String path) {
        this.path = path;
    }
    
    @Override
    public List<Image> load() {
        return linkImages(loadImages());
    }
    
    private List<Image> loadImages() {
        List<Image> list = new ArrayList<>();
        for (String file : new File(path).list()) {
            list.add(new ProxyImage(new FileImageLoader(path + "/" + file)));
        }
        return list;
    }

    private List<Image> linkImages(List<Image> images) {
        int length = images.size();
        for (int i = 0; i < length; i++) {
            Image image = images.get(i);
            image.setNextImage(images.get((i + 1) % length));
            image.setPrevImage(images.get((i + length - 1) % length));
        }
        return images;
    }
}
